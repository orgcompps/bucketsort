    .data
        .align 0
        str_1:  .asciiz "Reprovadoem"
        str_2:  .asciiz "Reprovadomeme"
    .text

.align 2
.globl main

main:
        la $a0, str_1
        la $a1, str_2
        jal strcmp

        move $a0, $v0
        li $v0, 1
        syscall

        li $v0, 10
        syscall

strcmp:         # Retorna negativo se str1 < str2, 0 se str1 == str2 e positivo se str1 > str2
                # $a0 contém o endereço da primeira string e $a1 o da segunda.

                addi $sp, $sp, -8
                sw $s0, 0($sp)
                sw $s1, 4($sp)

strcmp_charcmp:
                lb $s0, 0($a0)         # carrega o próximo caractere da str1
                lb $s1, 0($a1)         # carrega o próximo caractere da str2

                beq $s0, $zero, strcmp_end

                addi $a0, $a0, 1
                addi $a1, $a1, 1

                beq $s0, $s1, strcmp_charcmp

strcmp_end:
                sub $v0, $s0, $s1
                lw $s0, 0($sp)
                lw $s1, 4($sp)
                addi $sp, $sp, 8
                jr $ra

##############################
#     Fim de strcmp          #
##############################

strsize:        # Função para adquirir tamanho da string
                # $a0 contém o endereço da string, retorna em $v0 o tamanho da string

                # Empilha $a0
                addi $sp, $sp, -4
                sw $a0, 0($sp)

                li $v0, -1            # contador

strsize_count:
                lb $t1, 0($a0)
                addi $v0, $v0, 1
                addi $a0, $a0, 1

                bne $t1, $zero, strsize_count

                # Desempilha
                lw $a0, 0($sp)
                addi $sp, $sp, 4

                jr $ra

##############################
#     Fim de strsize         #
##############################
