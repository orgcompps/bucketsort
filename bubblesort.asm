##########################################
#					 #
#  Código Assembly MIPS para Bubblesort  #
#					 #
##########################################

	.text
	.align 2
	.globl main

main:	# &a0 = MAX
	# $a1 = endereço do vetor de strings

######################
#		     #
#     Bubblesort     #
#		     #
######################

bubblesort:
#######################################
#   Salvando registradores na pilha   #
#######################################
	addi $sp, $sp, -20	# Aloca espaço para os 5 registradores usados
	sw $s0, 0($sp)		# Salva cada um dos registradores na pilha
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $ra, 16($sp)

	move $s0, $a1		# $s0 = &vetor[]	
	move $s1, $a0		# $s1 = MAX
	move $s2, $zero		# i = 0

######################
#    Loop externo    # 
######################
loopi:	bge $s2, $s1, fimloopi	# Se i >= MAX vá para fimloopi
	move $s3, $s0		# Reinicia o loop interno. j = 0 

######################
#    Loop interno    #
######################
loopj:	addi $t3, $s1, -1	# reg. $t3 = (MAX-1)
	bge $s3, $t3, fimloopj	# Se j >= (MAX-1) vá para fimloopj

	mul $t0, $s3, 4		# reg. $t0 = j * 4 		Recebe deslocamento de bytes para vetor[j] em relação a vetor[]
	add $t1, $s0, $t0	# reg. $t1 = vetor[] + j	Incrementa deslocamento em bytes

	lw $t2, 0($t1)		# reg. $t2 = vetor[j]
	lw $t3, 4($t1)		# reg. $t3 = vetor[j+1]

###########################
#    Chamada da strcmp    #
###########################
	move $a0, $t2		# $a0 = vetor[j]
	move $a1, $t3		# $a1 = vetor[j+1]
	jal strcmp		# Chamada da strcmp. Retorno em $v0
	
	blez $v0, fimloopj	# Caso reg. $v0 <= 0 vá para fimloopj
				# Pula o swap entre as strings se já estão ordenadas

	# Swap entre os endereços de strings caso não ocorra desvio condicional
	
	mul $t0, $s3, 4		# reg. $t0 = j * 4 		Recebe deslocamento de bytes para vetor[j] em relação a vetor[]
	add $t1, $s0, $t0	# reg. $t1 = vetor[] + j	Incrementa deslocamento em bytes

	lw $t2, 0($t1)		# reg. $t2 = vetor[j]
	lw $t3, 4($t1)		# reg. $t3 = vetor[j+1]	
	
	sw $t3, 0($t1)		# Salva os registradores em posições invertida
	sw $t2, 4($t1)		# um na posição de meória do outro
			
	# Fim do swap

	addi $s3, $s3, 1	# Incrementa o loop interno j = j++
	j loopj			# Retorna para o loop interno

fimloopj:
	addi $s2, $s2, 1	# Incrementa o loop externo i = i++
	j loopi			# Retorna para o loop externo

fimloopi:
######################################
# Recuperando registradores na pilha #
######################################
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $ra, 16($sp)
	addi $sp, $sp, 20	# Desaloca o espaço reservado para os registradores salvos
	
	jr $ra			# Retorna para o trecho que fez desvio para o bubblesort
