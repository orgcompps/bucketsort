# Descrição em português #

Codigo Assembly MIPS para implementacao de um bucketsort

 Disciplina: Organizacao de Computadores Digitais 1

 Professor: Paulo Sergio Lopes de Souza

 Aluno PAE: Carlos Emilio de Andrade Cacho

 Grupo 11 - Alunos:

* Caio César Almeida Guimarães                8551497
* Helder de Melo Mendes                       8504351
* Henrique Cintra Miranda de Souza Aranha     8551434
* Lucas Eduardo Carreiro de Mello             8504218

 OBS: O arquivo foi testado utilizando qtspim 9.1.13-1 em linux

# English description #

Course: Computer Organization

Professor: Paulo Sergio Lopes de Souza

Supporting Student: Carlos Emilio de Andrade Cacho

Group 11 - Students: 

* Caio César Almeida Guimarães                8551497
* Helder de Melo Mendes                       8504351
* Henrique Cintra Miranda de Souza Aranha     8551434
* Lucas Eduardo Carreiro de Mello             8504218