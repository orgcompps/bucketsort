bucketSort:     # a0 -> tamanho do vetor, $a1 -> vetor de strings
                addi $sp, $sp, -24
                sw $a0, 0($sp)
                sw $a1, 4($sp)
                sw $s0, 8($sp)
                sw $s1, 12($sp)
                sw $s2, 16($sp)
                sw $ra, 20($sp)

                # Calcula quantos bytes terá cada balde.
                li $s0, 4
                mul $a0, $a0, $s0
                li $s1, 26              # Quantidade de baldes(letras no alfabeto)
                move $s2, $s1           # $s2: contador

bS_allocate:    # Aloca espaço para os baldes e coloca o endereço de cada balde na pilha.
                li $v0, 9
                syscall

                addi $sp, $sp, -8
                sw $zero, 4($sp)        # Número de strings no balde
                sw $v0, 0($sp)          # Endereço do balde

                addi $s2, $s2, -1       # Diminui o contador, pois criou-se um balde
                bgtz $s2, bS_allocate   # Se ainda deve ser criado mais baldes

                lw $t0, 208($sp)        # Recupera o tamanho do vetor de nomes(antigo $a0)
                sll $s0, $s0, 1         # Dobra, tornando 8

bS_separate:    # Separa os nomes em baldes a partir da primeira letra do nome
                lw $t2, 0($a1)          # Carrega o endereço da string
                lb $t3, 0($t2)          # Carrega o primeiro char da string.
                addi $t3, $t3, -65      # 65 corresponde ao char A na tabela ascii.

                mul $t3, $t3, $s0       # Desloca os 8 bytes que cada balde ocupa na memória

                add $t3, $t3, $sp       # t3 aponta para o seu balde correspondente na memória
                lw $t1, 4($t3)          # Carrega o número de strings presente no balde.
                lw $t4, 0($t3)          # Carrega o endereço do balde.

                add $t5, $t1, 1         # Incrementa o numero de strings no balde
                sw $t5, 4($t3)          # Salva esse tamanho na memória

                li $t5, 4               # Utilizado para pular 4 bytes para cada endereço
                mul $t1, $t1, $t5       # Multiplica a quantidade de strings por 4
                add $t4, $t4, $t1       # $t4 agora contém o endereço do proximo espaço vago no balde.

                sw $t2, 0($t4)          # Armazena a string no balde.

                addi $t0, $t0, -1       # Um nome ja foi posto no balde, entao diminui o tamanho
                addi $a1, $a1, 4        # Incrementa em 4 p/ adquirir o endereço da prox string
                bgtz $t0, bS_separate   # Enquanto houver nomes não colocados no balde, continua

                move $s0, $sp
                add $s1, $sp, -208      # Aponta para a primeira posição do vetor de nomes em $s1

bS_sort:        # Ordena cada um dos baldes
                lw $a1, 0($s0)          # Passa o endereço do balde.
                lw $a0, 4($s0)          # Passa o tamanho do balde.
                TODOjal bubbleSort          # Ordena o balde.

                add $s0, $s0, -8
                bne $s0, $s1, bS_sort

                lw $t0, 212($sp)        # Carrega o vetor de nomes
                li $t1, 26

bS_cat:         # Junta os baldes em um vetor ordenado de nomes
                addi $t1, $t1, -1

                lw $t2, 0($sp)          # Carrega o endereço do balde.
                lw $t3, 4($sp)          # Carrega a quantidade de strings do balde atual.
                addi $sp, $sp, 8

                bnez $t3, bS_insertb    # Se houver strings no balde, passa para o vetor de nomes

bS_condition:   bgtz $t1, bS_cat        # Enquanto houver baldes, continua concatenando
                j bS_exit

bS_insertb:     # Insere as strings no balde no vetor de nomes
                lw $t4, 0($t2)          # Carrega o endereço da string para $t4
                sw $t4, 0($t0)          # Armazena o endereço da string atual no vetor.
                addi $t2, $t2, 4        # Ajusta para a proxima posição do balde

                addi $t0, $t0, 4        # Passa para a proxima posição do vetor de nomes
                addi $t3, $t3, -1       # Indica que uma string ja foi passada para o vetor
                bgtz $t3, bS_insertb    # Enquanto houver string no balde, passa para o vetor

                j bS_condition

bS_exit:        # Desempilha
                lw $a0, 0($sp)
                lw $a1, 4($sp)
                lw $s0, 8($sp)
                lw $s1, 12($sp)
                lw $s2, 16($sp)
                lw $ra, 20($sp)
                addi $sp, $sp, 24

                # Retorna o vetor de nomes ordenado em $v0
                move $v0, $a1
                jr $ra

##############################
#     Fim de bucketSort      #
##############################
