strcpy:   # funcao que recebe uma string com endereco base em $a0 ( vetor v) e a copia ate que um '\0' seja encontrado para outra string com base em $v0(vetor x)


	
	add $t0,$a0,$zero         # $t1 = &v[0]
	add $t2,$v0,$zero      	  # $t2 = &x[0]

loop:	lbu $t1,0($t0)            # $t1 = v[i]
	sb  $t1,0($t2)            # x[i]=v[i]
	beq $t1,$zero,exit        # while (x[i] != '\0')
	addi $t2,$t2,1          # i++	
	addi $t0,$t0,1          # i++
	j loop
exit:


	jr $ra                    # retorna a funcao que fez a chamada de procedimentos


