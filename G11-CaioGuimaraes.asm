#
#   Trabalho 1
#
# Codigo Assembly MIPS para implementacao de um bucketsort
#
# Disciplina: Organizacao de Computadores Digitais 1
# Professor: Paulo Sergio Lopes de Souza
# Aluno PAE: Carlos Emilio de Andrade Cacho
#
# Grupo 11
# Alunos:
#           Caio César Almeida Guimarães                8551497
#           Helder de Melo Mendes                       8504351
#           Henrique Cintra Miranda de Souza Aranha     8551434
#           Lucas Eduardo Carreiro de Mello             8504218
#
# OBS: O arquivo foi testado utilizando qtspim 9.1.13-1 em linux
#

                .data
                .align 0
STR_DIG:        .asciiz "Digite o nome do arquivo: "
STR_FILENAME:   .space 1001             # O nome do arquivo pode ter ate 1000 caracteres

STR_OUTFILE:    .asciiz "saida.txt"
STR_SLASH:      .asciiz "/"

STR_GETOUT:     .asciiz "getout"        # Palavra-chave para fechar o programa

STR_FILEERROR:  .asciiz "Erro ao abrir o arquivo, digite outro nome.\n"
STR_NONAMES:    .asciiz "Arquivo sem strings, digite outro nome de arquivo.\n"

STR_NEWLINE:    .asciiz "\n"

BUFFER:         .space 2600             # Armazena todo o conteudo do arquivo (100 nomes de 25 letras, com os \n)
BUFFER2:        .space 2600             # Armazena todo o conteudo para ser escrito no arquivo (100 nomes de 25 letras, com os \n)

                .align 2
ARRAY_NAMES:    .space 400              # Vetor de nomes (armazena words com os endereços dos nomes)

                .text
                .align 2
                .globl main


######################
#                    #
#        main        #
#                    #
######################

main:           # Imprime STR_DIG na tela
                li $v0, 4               # Print_Str
                la $a0, STR_DIG
                syscall

                # Le o nome do arquivo
                li $v0, 8               # Read_Str
                la $a0, STR_FILENAME
                li $a1, 1000            # Numero de caracteres a serem lidos
                syscall

                la $a0, STR_FILENAME
                jal correct_string      # Adiciona um \0 ao final do nome
                move $a0, $v0           # Move a string corrigida para $a0

                # Testa se o usuario digitou "getout"
                la $a1, STR_GETOUT
                jal strcmp
                beq $v0, $zero, main_exit

                la $a0, STR_FILENAME    # $a0 possui o nome do arquivo
                li $a1, 0               # $a1 = 0 para abrir em modo leitura
                jal open_file

                # Verifica erro na abertura do arquivo
                la $a0, STR_FILEERROR    # Se houve erro, exibe essa mensagem
                li $t0, -1               # Usado para comparar com o ponteiro para o arquivo
                beq $v0, $t0, main_error # Se o arquivo nao foi aberto corretamente

                # Le o conteudo do arquivo
                move $a0, $v0           # Passa o ponteiro para o arquivo como argumento
                la $a1, BUFFER
                jal read_file

                # Fecha o arquivo em $a0
                jal close_file

                la $a0, BUFFER
                la $a1, ARRAY_NAMES
                jal create_array        # Transforma o conteudo de BUFFER em um vetor de nomes

                # Se não houver nomes no arquivo, indica o erro
                la $a0, STR_NONAMES         # Error a ser exibido se não houver nomes
                beq $v0, $zero, main_error  # Se o tamanho do vetor for zero, houve erro
                
                move $s0, $v0           # Salva o tamanho do vetor em $s0

                move $a0, $s0           # Tamanho do vetor
                jal bucketSort

                move $s1, $v0           # Vetor ordenado de endereços pros nomes
                
                la $s2, BUFFER2         # String que sera escrita no arquivo
                la $s3, STR_NEWLINE     # \n para ser concatenado no final dos nomes

                # Calcula a posicao final do vetor
                sll $s0, $s0, 2
                add $s0, $s1, $s0

                # Copia a primeira string do vetor para o BUFFER2
                lw $a0, 0($s1)
                move $a1, $s2
                jal strcpy

                addi $s1, $s1, 4

main_loop:      beq $s1, $s0, main_end  # Se o vetor chegou ao final
                
                # Concatena \n no final do ultimo nome em BUFFER
                move $a0, $s2
                move $a1, $s3
                move $a2, $s2
                jal strcat

                # Concatena o proximo nome em BUFFER
                move $a0, $s2
                lw $a1, 0($s1)
                move $a2, $s2
                jal strcat

                addi $s1, $s1, 4
                j main_loop

main_end:       # Gera o nome do arquivo de saida com o caminho correto
                la $a0, STR_FILENAME
                la $a1, STR_OUTFILE
                jal return_outfile

                # Abre o arquivo de saida no modo escrita
                move $a0, $v0
                li $a1, 1               # Modo escrita
                jal open_file

                # Escreve o conteudo de BUFFER2 no arquivo saida.txt
                move $a0, $v0           # Referencia para o arquivo
                move $a1, $s2           # Referencia para BUFFER2
                jal write_file

                # Fecha o arquivo em $a0
                jal close_file

                # Recebe um novo nome de arquivo
                j main

main_error:     # Imprime a mensagem de erro em $a0 e retorna ao inicio da main
                li $v0, 4
                syscall

                j main

main_exit:      # Encerra o programa
                li $v0, 10      # Exit
                syscall

##############################
#        Fim da main         #
##############################


######################
#                    #
#   correct_string   #
#                    #
######################

correct_string: # Substitui o byte 0x0a por 0(corresponde ao \0) na string apontado por $a0
                li $t0, 10              # Variavel utilizada para comparar com 0x0a (10), o "\n"
                move $t1, $a0           # Variavel temporaria para comparar char por char

loop_cs:        lb $t2, 0($t1)          # Carrega um char em $t2

                beq $t2, $t0, exit_cs   # Verifica se o byte atual eh 0x0a
                beq $t2, $zero, exit_cs # Verifica se o byte atual eh 0x00

                addi $t1, $t1, 1        # Incrementa o ponteiro para verificar o proximo byte

                j loop_cs

exit_cs:        sb $zero, 0($t1)        # Substitui o byte 0x0a ou 0x00 por 0x00
                
                move $v0, $a0           # Saida da funcao eh a string modificada
                jr $ra

##############################
#   Fim de correct_string    #
##############################

######################
#                    #
#   return_outfile   #
#                    #
######################

return_outfile: # Concatena o caminho do arquivo de entrada com o nome do arquivo de saida
                # $a0 possui a string com o nome do arquivo de entrada (que sera alterada)
                # $a1 possui a string com o nome do arquivo de saida

                # Empilha $a0, $a1 e $ra
                addi $sp, $sp, -12
                sw $a0, 0($sp)
                sw $a1, 4($sp)
                sw $ra, 8($sp)

                # Descobre o tamanho da string com o nome do arquivo de entrada
                jal strsize

                # Percorre o nome do arquivo de entrado do final para o comeco
                # parando na primeira "/" ou no inicio
                add $t0, $a0, $v0       # $t0 aponta para o final da string
                la $t1, STR_SLASH
                lb $t1, 0($t1)
                li $t2, 92              # Backslash("\") em ASCII
                
loop_ro:        lb $t3, 0($t0)          # Coloca o caracter em $t3 para comparar

                beq $t3, $t1, subs_ro   # Se for "/" substitui por "\0"
                beq $t3, $t2, subs_ro   # Se for "\" substitui por "\0"

                move $t3, $t0           # Auxiliar para verificar se a string foi totalmente vasculhada
                
                addi $t0, $t0, -1       # Aponta para o caracter anterior ao atual

                beq $t3, $a0, subs_ro   # Se a string nao possui barras
                
                j loop_ro

subs_ro:        # Substitui o caracter posterior por "\0"
                sb $zero, 1($t0)

exit_ro:        # Concatena o caminho do arquivo com o nome do arquivo de saida
                move $a2, $a0           # Salva a concatenacao na memoria apontado por $a0
                jal strcat

                # Desempilha
                lw $a0, 0($sp)
                lw $a1, 4($sp)
                lw $ra, 8($sp)
                addi $sp, $sp, 12

                # A saída é o endereco apontado por $a0
                move $v0, $a0

                jr $ra

##############################
#   Fim de return_outfile    #
##############################

######################
#                    #
#     open_file      #
#                    #
######################

open_file:      # Abre um arquivo com o nome apontado por $a0
                # $a1 = 0 => MODO LEITURA
                # $a1 = 1 => MODO ESCRITA

                # Empilha $a0 e $a1
                addi $sp, $sp, -8
                sw $a0, 0($sp)
                sw $a1, 4($sp)

                # Abre o arquivo com o nome que esta em $a0
                li $v0, 13              # Open
                beq $a1, $zero, read_of # Se eh em modo leitura

                # Abre o arquivo no modo escrita, criando-o
                li $a1, 0x42            # flags: Create=0x40 OR Read/WRite=0x2
                li $a2, 0x180           # permissions: Read=0x100 OR Write=0x80

                j exit_of


read_of:        # Abre o arquivo no modo leitura
                li $a1,0                # flags: Read=0x0
                li $a2,0x100            # permissions: Read=0x100

exit_of:        syscall                 # Abre o arquivo ($v0 = 13)

                # Desempilha $a0 e $a1
                lw $a0, 0($sp)
                lw $a1, 4($sp)
                addi $sp, $sp, 8

                # Retorna o ponteiro para o arquivo em $v0
                jr $ra

##############################
#      Fim de open_file      #
##############################


######################
#                    #
#     close_file     #
#                    #
######################

close_file:     # Fecha o arquivo apontado por $a0
                li $v0, 16              # Close
                syscall

                jr $ra

##############################
#      Fim de close_file     #
##############################


######################
#                    #
#     read_file      #
#                    #
######################

read_file:      # Le o conteudo do arquivo apontado por $a0
                # e salva no BUFFER em $a1

                # Empilha $a0 e $a1
                addi $sp, $sp, -8
                sw $a0, 0($sp)
                sw $a1, 4($sp)

                # Le o conteudo do arquivo apontado por $a0
                # para o buffer em $a1
                li $v0, 14              # Read
                li $a2, 2600            # Le ate 2600 bytes
                syscall

                # Desempilha $a0 e $a1
                lw $a0, 0($sp)
                lw $a1, 4($sp)
                addi $sp, $sp, 8

                jr $ra

##############################
#      Fim de read_file      #
##############################


######################
#                    #
#     write_file     #
#                    #
######################

write_file:     # Escreve no arquivo apontado por $a0
                # o conteudo salvo no BUFFER em $a1

                # Empilha $a0, $a1, $ra
                addi $sp, $sp, -12
                sw $a0, 0($sp)
                sw $a1, 4($sp)
                sw $ra, 8($sp)

                move $t0, $a0

                # Descobre o tamanho do BUFFER
                move $a0, $a1
                jal strsize

                # Escreve o conteudo apontado por $a1 no arquivo
                move $a0, $t0
                move $a2, $v0           # Tamanho do vetor
                li $v0, 15              # Write
                syscall

                # Desempilha $a0
                lw $a0, 0($sp)
                lw $a1, 4($sp)
                lw $ra, 8($sp)
                addi $sp, $sp, 12

                jr $ra

##############################
#      Fim de write_file     #
##############################


######################
#                    #
#    create_array    #
#                    #
######################

create_array:   # Cria um vetor de nomes a partir dos nomes em BUFFER
                # $a0 eh o ponteiro para o BUFFER e $a1 eh o ponteiro para o vetor

                # Empilha $a0 e $a1
                addi $sp, $sp, -8
                sw $a0, 0($sp)
                sw $a1, 4($sp)

                li $t1, 10              # Variavel utilizada para comparar com 0x0a (10), o "\n"
                li $v0, 0               # Inicia o tamanho do vetor (Saida da funcao) como 0

loop1_ca:       # Coloca a posicao do nome(string) no vetor de nomes($a1)
                lb $t0, 0($a0)          # Carrega o primeiro caracter apontado por $a0
                beq $t0, $zero, exit_ca # Se for \0, finaliza a funcao, evitando erros quando o arquivo nao tiver nomes

                sw $a0, 0($a1)          # Coloca no vetor a posicao do nome
                addi $a1, $a1, 4        # Pula para a proxima posicao do vetor
                addi $v0, $v0, 1        # Indica que o tamanho do vetor aumentou

loop2_ca:       # Percorre o BUFFER em busca de \n indicando proximos nomes
                beq $t0, $t1, subs_ca   # Se houver um \n, o nome acabou
                beq $t0, $zero, exit_ca # Se houver um \0, o BUFFER acabou

                addi $a0, $a0, 1        # Senao, passa para a proxima letra
                lb $t0, 0($a0)          # Carrega a letra em $t0

                j loop2_ca              # Volta ao comeco do loop

subs_ca:        # Substitui o \n por um \0 indicando fim do nome
                sb $zero, 0($a0)

                addi $a0, $a0, 1        # Passa para o proximo nome
                j loop1_ca              # Volta ao comeco do primeiro loop

exit_ca:        # Desempilha $a0 e $a1
                lw $a0, 0($sp)
                lw $a1, 4($sp)
                addi $sp, $sp, 8
                jr $ra

##############################
#     Fim de fill_array      #
##############################


######################
#                    #
#     strcmp         #
#                    #
######################

strcmp:         # Retorna negativo se str1 < str2, 0 se str1 == str2 e positivo se str1 > str2
                # $a0 contém o endereço da primeira string e $a1 o da segunda.
                
                addi $sp, $sp, -8
                sw $s0, 0($sp)
                sw $s1, 4($sp)

strcmp_charcmp:
                lb $s0, 0($a0)         # carrega o próximo caractere da str1
                lb $s1, 0($a1)         # carrega o próximo caractere da str2

                beq $s0, $zero, strcmp_end

                addi $a0, $a0, 1
                addi $a1, $a1, 1

                beq $s0, $s1, strcmp_charcmp

strcmp_end: 
                sub $v0, $s0, $s1
                lw $s0, 0($sp)
                lw $s1, 4($sp)
                addi $sp, $sp, 8
                jr $ra

##############################
#     Fim de strcmp          #
##############################


######################
#                    #
#     strsize        #
#                    #
######################

strsize:        # Função para adquirir tamanho da string
                # $a0 contém o endereço da string, retorna em $v0 o tamanho da string
                
                # Empilha $a0
                addi $sp, $sp, -4
                sw $a0, 0($sp)

                li $v0, -1            # contador

strsize_count:
                lb $t1, 0($a0)
                addi $v0, $v0, 1
                addi $a0, $a0, 1
                
                bne $t1, $zero, strsize_count

                # Desempilha
                lw $a0, 0($sp)
                addi $sp, $sp, 4

                jr $ra

##############################
#     Fim de strsize         #
##############################


######################
#                    #
#     strcpy         #
#                    #
######################

strcpy:         # funcao que recebe uma string com endereco base em $a0 ( vetor v)
                # e a copia ate que um '\0' seja encontrado para outra string com base em $a1(vetor x)
                # retornando o endereco de $a1 em $v0

                move $v0, $a1

                add $t0, $a0, $zero     # $t0 = &v[0]
                add $t2, $a1, $zero     # $t2 = &x[0]

loop_scpy:      lb $t1, 0($t0)          # $t1 = v[i]
                sb $t1, 0($t2)          # x[i] = v[i]
                beq $t1,$zero,exit_scpy # while (x[i] != '\0')
                addi $t2,$t2,1          # i++
                addi $t0,$t0,1          # i++
                
                j loop_scpy
exit_scpy:
                jr $ra                  # retorna a funcao que fez a chamada de procedimentos

##############################
#     Fim de strcpy          #
##############################


######################
#                    #
#     strcat         #
#                    #
######################

strcat:         # função assume endereço de uma string com endereço base em $a0(vetor v)
                # e de outra em $a1(vetor x). 
                # Concatena as duas strings em uma nova string em $a2 (vetor y)


                add $t0,$zero,$zero     # i = 0
                add $t3,$zero,$zero     # j = 0
                
                move $v0, $a2

loop1_cat:      add $t1, $a0, $t0       # $t1=  &v[i]
                lb $t2, 0($t1)          # t2 = v[i]
                beq $t2,$zero,loop2_cat # while (v[i] != '\0')   
                add $t4,$v0,$t0         # $t4 = &y[i]
                sb $t2,0($t4)           # y[i]= v[i];
                addi $t0,$t0,1          # i ++;
            
                j loop1_cat

loop2_cat:      add $t1, $a1, $t3       # $t1 = &x[j]
                lb $t2, 0($t1)          # $t2 = x[j]         
                add $t4, $v0, $t0       # $t4 = &y[i]
                sb $t2, 0($t4)          # y[i]=x[j]
                beq $t2,$zero,exit_cat  # (while y[j]!='\0' )
                
                addi $t3,$t3,1          # j ++
                addi $t0,$t0,1          # i ++
                j loop2_cat
exit_cat:
                jr $ra                  # retorna para o ponto de chamada

##############################
#     Fim de strcat          #
##############################


######################
#                    #
#     bucketsort     #
#                    #
######################

bucketSort:     # a0 -> tamanho do vetor, $a1 -> vetor de strings
                addi $sp, $sp, -24
                sw $a0, 0($sp)
                sw $a1, 4($sp)
                sw $s0, 8($sp)
                sw $s1, 12($sp)
                sw $s2, 16($sp)
                sw $ra, 20($sp)

                # Calcula quantos bytes terá cada balde.
                li $s0, 4
                mul $a0, $a0, $s0
                li $s1, 26              # Quantidade de baldes(letras no alfabeto)
                move $s2, $s1           # $s2: contador

bS_allocate:    # Aloca espaço para os baldes e coloca o endereço de cada balde na pilha.
                li $v0, 9
                syscall

                addi $sp, $sp, -8
                sw $zero, 4($sp)        # Número de strings no balde
                sw $v0, 0($sp)          # Endereço do balde

                addi $s2, $s2, -1       # Diminui o contador, pois criou-se um balde
                bgtz $s2, bS_allocate   # Se ainda deve ser criado mais baldes

                lw $t0, 208($sp)        # Recupera o tamanho do vetor de nomes(antigo $a0)
                sll $s0, $s0, 1         # Dobra, tornando 8

bS_separate:    # Separa os nomes em baldes a partir da primeira letra do nome
                lw $t2, 0($a1)          # Carrega o endereço da string
                lb $t3, 0($t2)          # Carrega o primeiro char da string.
                addi $t3, $t3, -65      # 65 corresponde ao char A na tabela ascii.
                
                mul $t3, $t3, $s0       # Desloca os 8 bytes que cada balde ocupa na memória
                
                add $t3, $t3, $sp       # t3 aponta para o seu balde correspondente na memória
                lw $t1, 4($t3)          # Carrega o número de strings presente no balde.
                lw $t4, 0($t3)          # Carrega o endereço do balde.

                add $t5, $t1, 1         # Incrementa o numero de strings no balde
                sw $t5, 4($t3)          # Salva esse tamanho na memória

                li $t5, 4               # Utilizado para pular 4 bytes para cada endereço
                mul $t1, $t1, $t5       # Multiplica a quantidade de strings por 4
                add $t4, $t4, $t1       # $t4 agora contém o endereço do proximo espaço vago no balde.

                sw $t2, 0($t4)          # Armazena a string no balde.

                addi $t0, $t0, -1       # Um nome ja foi posto no balde, entao diminui o tamanho
                addi $a1, $a1, 4        # Incrementa em 4 p/ adquirir o endereço da prox string
                bgtz $t0, bS_separate   # Enquanto houver nomes não colocados no balde, continua

                move $s0, $sp
                add $s1, $sp, 208      # Aponta para o primeiro balde na memória

bS_sort:        # Ordena cada um dos baldes
                lw $a1, 0($s0)          # Passa o endereço do balde.
                lw $a0, 4($s0)          # Passa o tamanho do balde.
                jal bubblesort          # Ordena o balde.

                add $s0, $s0, 8
                bne $s0, $s1, bS_sort

                lw $t0, 212($sp)        # Carrega o vetor de nomes
                li $t1, 26

bS_cat:         # Junta os baldes em um vetor ordenado de nomes
                addi $t1, $t1, -1

                lw $t2, 0($sp)          # Carrega o endereço do balde.
                lw $t3, 4($sp)          # Carrega a quantidade de strings do balde atual.
                addi $sp, $sp, 8

                bnez $t3, bS_insertb    # Se houver strings no balde, passa para o vetor de nomes

bS_condition:   bgtz $t1, bS_cat        # Enquanto houver baldes, continua concatenando
                j bS_exit

bS_insertb:     # Insere as strings no balde no vetor de nomes
                lw $t4, 0($t2)          # Carrega o endereço da string para $t4
                sw $t4, 0($t0)          # Armazena o endereço da string atual no vetor.
                addi $t2, $t2, 4        # Ajusta para a proxima posição do balde

                addi $t0, $t0, 4        # Passa para a proxima posição do vetor de nomes
                addi $t3, $t3, -1       # Indica que uma string ja foi passada para o vetor
                bgtz $t3, bS_insertb    # Enquanto houver string no balde, passa para o vetor
                
                j bS_condition

bS_exit:        # Desempilha
                lw $a0, 0($sp)
                lw $a1, 4($sp)
                lw $s0, 8($sp)
                lw $s1, 12($sp)
                lw $s2, 16($sp)
                lw $ra, 20($sp)
                addi $sp, $sp, 24

                # Retorna o vetor de nomes ordenado em $v0
                move $v0, $a1
                jr $ra

##############################
#     Fim de bucketSort      #
##############################


######################
#                    #
#     bubblesort     #
#                    #
######################

bubblesort:     # Salvando registradores na pilha
                addi $sp, $sp, -20      # Aloca espaço para os 5 registradores usados
                sw $s0, 0($sp)          # Salva cada um dos registradores na pilha
                sw $s1, 4($sp)
                sw $s2, 8($sp)
                sw $s3, 12($sp)
                sw $ra, 16($sp)

                move $s0, $a1           # $s0 = &vetor[]
                move $s1, $a0           # $s1 = MAX
                move $s2, $zero         # i = 0

loopi:          # Loop externo
                bge $s2, $s1, fimloopi  # Se i >= MAX vá para fimloopi
                move $s3, $zero         # Reinicia o loop interno. j = 0 

loopj:          # Loop interno
                addi $t3, $s1, -1       # $t3 = (MAX-1)
                bge $s3, $t3, fimloopj  # Se j >= (MAX-1) vá para fimloopj

                mul $t0, $s3, 4         # $t0 = j * 4  Recebe deslocamento de bytes para vetor[j] em relação a vetor[]
                add $t1, $s0, $t0       # $t1 = &vetor[] + j   Incrementa deslocamento em bytes

                lw $t2, 0($t1)          # $t2 = vetor[j]
                lw $t3, 4($t1)          # $t3 = vetor[j+1]

                ###########################
                #    Chamada da strcmp    #
                ###########################
                move $a0, $t2           # $a0 = vetor[j]
                move $a1, $t3           # $a1 = vetor[j+1]
                jal strcmp              # Chamada da strcmp. Retorno em $v0

                blez $v0, fimswap       # Caso reg. $v0 <= 0 vá para fimswap
                # Pula o swap entre as strings se já estão ordenadas

                # Swap entre os endereços de strings caso não ocorra desvio condicional
                mul $t0, $s3, 4         # $t0 = j * 4   Recebe deslocamento de bytes para vetor[j] em relação a vetor[]
                add $t1, $s0, $t0       # $t1 = vetor[] + j    Incrementa deslocamento em bytes

                lw $t2, 0($t1)          # $t2 = vetor[j]
                lw $t3, 4($t1)          # $t3 = vetor[j+1]

                sw $t3, 0($t1)          # Salva os registradores em posições invertida
                sw $t2, 4($t1)          # um na posição de memória do outro

fimswap:        # Fim do swap

                addi $s3, $s3, 1        # Incrementa o loop interno j = j++
                j loopj                 # Retorna para o loop interno

fimloopj:
                addi $s2, $s2, 1        # Incrementa o loop externo i = i++
                j loopi                 # Retorna para o loop externo

fimloopi:       # Recuperando registradores na pilha
                lw $s0, 0($sp)
                lw $s1, 4($sp)
                lw $s2, 8($sp)
                lw $s3, 12($sp)
                lw $ra, 16($sp)
                addi $sp, $sp, 20       # Desaloca o espaço reservado para os registradores salvos

                jr $ra                  # Retorna para o trecho que fez desvio para o bubblesort
                
##############################
#     Fim de bubblesort      #
##############################