strcat : # função assume endereço de uma string com endereço base em $a0(vetor v), e de outra em $a1(vetor x) . o objetivo desta função é concatenar as duas em uma nova string(vetor y)


	add $t0,$zero,$zero        # i = 0
	add $t3,$zero,$zero        #j = 0

	

loop1: 	add $t1,$a0,$t0              # $t1=  &v[i]
	lbu $t2,0($t1)               # t2 = v[i]
	beq $t2,$zero,exit1          # while (v[i] != '\0')   
	add $t4,$v0,$t0		     # $t4 = &y[i]
	sb $t2,0($t4)                # y[i]= v[i];
	addi $t0,$t0,1                # i ++;	              
	j loop1
exit1:



loop2:  add $t1,$a1,$t3             # $t1 = &x[j]
	lbu $t2,0($t1)               # $t2 = x[j]         
	add $t4,$v0,$t0              # $t4 = &y[i]	
	sb $t2,0($t4)		     # y[i]=x[j]	
	beq $t2,$zero,exit2           # (while y[j]!='\0' )
	addi $t3,$t3,1                # j ++
	addi $t0,$t0,1                # i ++	        
	j loop2
exit2:

	jr $ra                      # retorna uma string com a base do endereco no registrador $v0 à função que fez a chamada de procedimentos
