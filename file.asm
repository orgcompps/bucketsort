#
#	Trabalho 1
#
# Codigo Assembly MIPS para implementacao de um bucketsort
#
# Disciplina: Organizacao de Computadores Digitais 1
# Professor: Paulo Sergio Lopes de Souza
# Aluno PAE: Carlos Emilio de Andrade Cacho 
#
# Alunos:
#	
#	
#	
#	
#
		.data
		.align 0
STR_DIG:	.asciiz "Digite o nome do arquivo: "
STR_FILENAME:	.space 32	 	# O nome do arquivo pode ter ate 31 caracteres
BUFFER:		.space 2600		# Armazena todo o conteudo do arquivo (100 nomes de 25 letras, com os \n)
ARRAY_NAMES:	.space 400		# Vetor de nomes
	
		.text
		.align 2
		.globl main

main:		# Imprime STR_DIG na tela
		li $v0, 4		# Print_Str
		la $a0, STR_DIG
		syscall

		# Le o nome do arquivo
		li $v0, 8		# Read_Str
		la $a0, STR_FILENAME
		li $a1, 31		# Numero de caracteres a serem lidos
		syscall

		la $a0, STR_FILENAME
		jal correct_string	# Adiciona um \0 ao final do nome
	
		#TODO compara STR_NAME para saber se eh "getout"

		#TODO Se nao for getout
		la $a0, STR_FILENAME	# $a0 possui o nome do arquivo
		li $a1, 0		# $a1 = 0 para abrir em modo leitura
		jal open_file
		move $t0, $v0		# $t0 eh um ponteiro para o arquivo aberto

		# Le o conteudo do arquivo
		move $a0, $t0		# Passa o ponteiro para o arquivo como argumento
		jal read_file

		# Fecha o arquivo em $a0
		jal close_file

		la $a0, BUFFER
		la $a1, ARRAY_NAMES
		jal fill_array		# Transforma o conteudo de BUFFER em um vetor de nomes
		move $v0, $s0		# Salva o tamanho do vetor em $s0

		# Recebe um novo nome de arquivo
		# TODO j main

main_exit:	# Encerra o programa
		li $v0, 10		# Exit
		syscall
	
##############################
#        Fim da main         #
##############################

correct_string: # Substitui o byte 0x0a por 0(corresponde ao \0) na string apontado por $a0
		li $t0, 10		# Variavel utilizada para comparar com 0x0a (10), o "\n"
	
loop_cs:	lb $t1, 0($a0)
	
		beq $t1, $t0, exit_cs	# Verifica se o byte atual eh 0x0a
		beq $t1, $zero, exit_cs	# Verifica se o byte atual eh 0x00

		addi $a0, $a0, 1		# Incrementa o ponteiro para verificar o proximo byte

		j loop_cs

exit_cs:	sb $zero, 0($a0)	# Substitui o byte 0x0a ou 0x00 por 0x00

		jr $ra

##############################
#   Fim de correct_string    #
##############################
	
open_file:	# Abre um arquivo com o nome apontado por $a0
		# $a1 = 0 => MODO LEITURA
		# $a2 = 1 => MODO ESCRITA
	
		# Empilha $a0 e $a1
		addi $sp, $sp, -8
		sw $a0, 0($sp)
		sw $a1, 4($sp)
	
		# Abre o arquivo com o nome que esta em $a0
		li $v0, 13		# Open
		beq $a1, $zero, read_of # Se eh em modo leitura

write_of:	# Abre o arquivo no modo escrita, criando-o
		li $a1, 0x4042		# flags: Text=0x4000 OR Create=0x40 OR Read/WRite=0x2
		li $a2, 0x180		# permissions: Read=0x100 OR Write=0x80

		j exit_of


read_of:	# Abre o arquivo no modo leitura
		li $a1,0x4000	# flags: Text=0x4000 OR Read=0x0
		li $a2,0x100	# permissions: Read=0x100

exit_of:	syscall		# Abre o arquivo ($v0 = 13)
	
		# Desempilha $a0 e $a1
		lw $a0, 0($sp)
		lw $a1, 4($sp)
		addi $sp, $sp, 8
	
		# Retorna o ponteiro para o arquivo em $v0
		jr $ra

##############################
#      Fim de open_file      #
##############################
	
close_file:	# Fecha o arquivo apontado por $a0
		li $v0, 16		# Close
		syscall
		
		jr $ra

##############################
#      Fim de close_file     #
##############################

read_file:	# Empilha $a0
		addi $sp, $sp, -4
		sw $a0, 0($sp)
	
		# Le o conteudo do arquivo apontado por $a0
		li $v0, 14		# Read
		la $a1, BUFFER		# Salva o conteudo no espaco alocado em BUFFER
		li $a2, 2600		# Le ate 2600 bytes
		syscall

		# Desempilha $a0
		lw $a0, 0($sp)
		addi $sp, $sp, 4

		jr $ra

##############################
#      Fim de read_file      #
##############################

create_array:	# Cria um vetor de nomes a partir dos nomes em BUFFER
		# $a0 eh o ponteiro para o BUFFER e $a1 eh o ponteiro para o vetor

		# Empilha $a0 e $a1
		addi $sp, $sp, -8
		sw $a0, 0($sp)
		sw $a1, 4($sp)

		li $t1, 10		# Variavel utilizada para comparar com 0x0a (10), o "\n"
		li $v0, 0		# Inicia o tamanho do vetor (Saida da funcao) como 0

loop1_ca:	# Coloca a posicao do nome(string) no vetor de nomes($a1)
		lb $t0, 0($a0)		# Carrega o primeiro caracter apontado por $a0
		bne $t0, $zero, exit_ca	# Se for \0, finaliza a funcao, evitando erros quando o arquivo nao tiver nomes

		move $a1, $a0		# Coloca no vetor a posicao do nome
		addi $a1, $a1, 4	# Pula para a proxima posicao do vetor
		addi $v0, $v0, 1	# Indica que o tamanho do vetor aumentou
		
loop2_ca:	# Percorre o BUFFER em busca de \n indicando proximos nomes
		beq $t0, $t1, subs_ca	# Se houver um \n, o nome acabou
		beq $t0, $zero, exit_ca	# Se houver um \0, o BUFFER acabou

		addi $a0, $a0, 1	# Senao, passa para a proxima letra
		lb $t0, 0($a0)		# Carrega a letra em $t0

		j loop2_ca		# Volta ao comeco do loop

subs_ca:	# Substitui o \n por um \0 indicando fim do nome
		sb $zero, 0($a0)

		addi $a0, $a0, 1	# Passa para o proximo nome
		j loop1_ca		# Volta ao comeco do primeiro loop

exit_ca:	# Desempilha $a0 e $a1
		lw $a0, 0($sp)
		lw $a1, 4($sp)
		addi $sp, $sp, 8

		jr $ra

##############################
#     Fim de fill_array      #
##############################
